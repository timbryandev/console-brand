# Console Brand

**NOTE** This project's only purpose is to allow me to test building and and publishing NPM packages and shouldn't actually be used by anyone, ever!

## Prerequisites

- Node
- NPM

## Installation

- Currently, no installation is required as all code is native JavaScript with no packages or imports.

## Development

Running `npm start` should be all you need.

This will create an imported instance of the code and run it to test the console output.

## Deploy

Running either `npm run publish` or `npm publish --access=public` will publish the package to <https://npmjs.com/~timbryandev> as a scoped package with public visibility.
